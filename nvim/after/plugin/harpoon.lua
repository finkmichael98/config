local harpoon = require("harpoon")

-- local settings = {
--     save_on_toggle=true
-- }

-- harpoon:setup()

harpoon:setup({
    settings = {
        save_on_toggle = true,
    },
})

vim.keymap.set("n", "<leader>a", function() harpoon:list():add() end)
vim.keymap.set("n", "<leader><tab>", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end)

vim.keymap.set("n", "<leader>1", function() harpoon:list():select(1) end)
vim.keymap.set("n", "<leader>2", function() harpoon:list():select(2) end)
vim.keymap.set("n", "<leader>3", function() harpoon:list():select(3) end)
vim.keymap.set("n", "<leader>4", function() harpoon:list():select(4) end)

-- Toggle to next buffer stored within Harpoon list
vim.keymap.set("n", "<M-n>", function() harpoon:list():next({ui_nav_wrap=true}) end)
vim.keymap.set("n", "<M-b>", function() harpoon:list():prev({ui_nav_wrap=true}) end)
